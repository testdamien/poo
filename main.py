from __future__ import annotations
from abc import ABC, abstractmethod


class InvalideExeption(Exception):
    pass

class Element:
    
    value: int
    next: Element
    def __init__(self, value:int, next: Element=None):
        self.value = value
        self.next = None
        
    def __str__(self)->str:
        return str(self.value)
        pass
    
    def __eq__(self, e :int)->bool:
        return self.value == e

class ChainList(ABC):
    
    def __init__(self) -> None:
        pass
    
    @abstractmethod
    def insert(self, e):
        pass

    
    def is_empty(self) -> bool:
        return self.top is None 
 

    def __len__(self)->int:
        pass
    
    def __str__(self) -> str:
        r=""
        e=self.top
        while e is not None:
            r=r+str(e)+'->'
            e=e.next
        return r
        
    
    def delete(self, e:Element):
            if self.top == e:
                self.top = self.top.next
            else:
                previous=self.top
                current=previous.next
                while current is not None and not current ==e:
                    previous, current = current, previous.next
                    
                if current is None:
                    raise InvalideExeption("NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO i'm not here")
                else:
                    previous;next=current.next
                    



class File(ChainList):
    
    def __init__(self):
        self.top = None

    def insert(self,e:Element):
        new_element = Element(e)
        if self.top is None:
            self.top = new_element
        else:
            current = self.top
            while current.next:
                current = current.next
            current.next = new_element

class OrderedList(ChainList):
    
    def __init__(self):
        self.top = None

    def insert(self,e:Element):
        new_element = Element(value)
        if self.top is None or self.top.value > value:
            new_element.next = self.top
            self.top = new_element
        else:
            current = self.top
            while current.next and current.next.value < value:
                current = current.next
            new_element.next = current.next
            current.next = new_element



f = File()
f.insert(1)
f.insert(4)
f.insert(5)
f.insert(6)
f.insert(2)
f.delete(1)
print(f)


